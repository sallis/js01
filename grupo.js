function Grupo(c, n){
  var _clave = c;
  var _nombre = n;
  var _alumnos = [];
  var _docentes = [];
  var _materias = [];
  
  function _getClave(){
    return _clave;
  }
  
  function _setClave(c){
    _clave = c;
  }

  function _getNombre(){
    return _nombre;
  }
  
  function _setNombre(n){
    _nombre = n;
  }
  
  function _agregarA(a){
    if(_existeA(a.getnControl()))
      console.log("El alumno ya existe");
    else if(_alumnos.length < 30)
      _alumnos.push(a);
    else
      console.log("El grupo ya tiene 30 alumnos");
  }
  
  function _agregarD(d){
    if(_existeD(d.getrfc()))
      console.log("El docente ya existe");
    else if(_docentes.length < 7)
      _docentes.push(d);
    else
      console.log("No se puede agregar un docente mas");
  }
  
  function _agregarM(m){
    if(_existeM(m.getClave()))
      console.log("La materia ya existe");
    else if(_docentes.length < 7)
      _materias.push(m);
    else
      console.log("No se puede agregar mas materias");
  }
  
  function _borrarA(a){
   if(_existeA(a.getnControl()))
      _alumnos.splice(i, 1);
    else
      console.log("No existe Alumno...");  
  }
  
  function _borrarD(d){
   if(_existeD(d.getrfc()))
      _docentes.splice(i, 1);
    else
      console.log("No existe Docente...");  
  }
  
  function _borrarM(m){
   if(_existeM(m.getClave()))
      _materias.splice(i, 1);
    else
      console.log("No existe Materia...");  
  }
  
  function _editarA(nc,n,ap,am,g){
   var i = _buscar(nc);
   _alumnos[i].setnControl(nc);
   _alumnos[i].setNombre(n);
   _alumnos[i].setApellidoP(ap);
   _alumnos[i].setApellidoM(am);
   _alumnos[i].setGenero(g); 
  }
  
  
  function _buscar(nc){ 
    for(var i = 0;i< _lista.length; i++){
      if(_alumnos[i].getnControl() === nc)
        return i;
      else
        console.log("No encontrado...");
    } 
  }
  
  function _existeA(nc){  
    for(var i = 0;i< _alumnos.length; i++){
      if(_alumnos[i].getnControl() === nc)
        return true;
    }
    return false;
  }
  
  function _existeD(rfc){  
    for(var i = 0;i< _docentes.length; i++){
      if(_docentes[i].getrfc() === rfc)
        return true;
    }
    return false;
  }
  
  function _existeM(c){  
    for(var i = 0;i< _materias.length; i++){
      if(_materias[i].getClave() === c)
        return true;
    }
    return false;
  }
  
  function _alumnos(){
    for(var i = 0;i< _alumnos.length; i++)
      console.log("Numero de control:"+_alumnos[i].getnControl()+" Nombre:"+_alumnos[i].getNombre() +" ApellidoP:"+_alumnos[i].getApellidoP()+" ApellidoM:"+_alumnos[i].getApellidoM()+" Genero:"+_alumnos[i].getGenero());
  }
  
  function _docentes(){
    for(var i = 0;i< _docentes.length; i++)
      console.log("RFC:"+_docentes[i].getrfc()+" Nombre:"+_docentes[i].getNombre() +" ApellidoP:"+_docentes[i].getApellidoP()+" ApellidoM:"+_docentes[i].getApellidoM()+" Genero:"+_docentes[i].getGenero());
  }
  
  function _materias(){
    for(var i = 0;i< _materias.length; i++)
      console.log("Clave:"+_materias[i].getClave()+" Nombre:"+_materias[i].getNombre());
   
  return{
    "agregarA": _agregarA,
    "agregarD": _agregarD,
    "agregarM": _agregarM,
    "borrarA": _borrarA,
    "borrarD": _borrarD,
    "borrarM": _borrarM,
    "editar": _editar,
    "buscar": _buscar,
    "existeA": _existeA,
    "existeD": _existeD,
    "existeM": _existeM,
    "alumnos": _alumnos,
    "docentes": _docentes,
    "materias": _materias
  };
}
