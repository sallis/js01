function Alumno(nc, n, ap, am, g){
  var _ncontrol = nc;
  var _nombre = n;
  var _apellidoP = ap;
  var _apellidoM = am;
  var _genero = g;
  var _calif = [];
  
  function _getnControl(){
    return _ncontrol;
  }
  
  function _setnControl(nc){
    _ncontrol = nc;
  }

  function _getNombre(){
    return _nombre;
  }
  
  function _setNombre(n){
    _nombre = n;
  }
  
  function _getApellidoP(){
    return _apellidoP;
  }
  
  function _setApellidoP(ap){
    _apellidoP = ap;
  }
  
  function _getApellidoM(){
    return _apellidoM;
  }
  
  function _setApellidoM(am){
    _apellidoM = am;
  }
  
  function _getGenero(){
    return _genero;
  }
  
  function _setGenero(g){
    _genero = g;
  }
  
  return{
    "getnControl": _getnControl,
    "getNombre": _getNombre,
    "getApellidoP": _getApellidoP,
    "getApellidoM": _getApellidoM,
    "getGenero": _getGenero,
    "calif": _calif
  };
}
