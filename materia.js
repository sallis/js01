function Materia(c, n){
  var _clave = c;
  var _nombre = n;
  var _lista = [];

  function _getClave(){
    return _clave;
  }
  
  function _setClave(c){
    _clave = c;
  }

  function _getNombre(){
    return _nombre;
  }
  
  function _setNombre(n){
    _nombre = n;
  }
  
  return{
    "getClave": _getClave,
    "getNombre": _getNombre,
    "lista": _lista
  };
}
